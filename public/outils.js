document.getElementById("envoyer").addEventListener("click", function(event){
    event.preventDefault();
    var user = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var req = new XMLHttpRequest();
	req.open('POST', '/login?user='+user+'&password='+password, true);

	req.onreadystatechange = function (aEvt) {
	  if (req.readyState == 4) {
	     if(req.status == 200)
	     {
	     	document.getElementById("afficheToken").innerHTML = req.responseText;
	     }
	     else
	     {	
	     	document.getElementById("afficheToken").innerHTML = "Erreur ce user n'existe pas"; 	
	     }
	  }
	};
	req.send(null);
});